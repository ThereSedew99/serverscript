# -*- coding: UTF-8 -*-
from django.http import HttpResponse
import datetime
import time

# turn on mysql debug mode
import cgitb
import pymysql
cgitb.enable()

from django.template import Template,Context
from django.template.loader import get_template
from django.shortcuts import render

class ParsedWord(object) :
	def __init__(self, word=None, cnt=None):
		self.word = word
		self.cnt = cnt

def connect_wordDB():
	conn = pymysql.connect (
        	db='test',
        	user='root',
        	passwd='y2seul',
        	host='localhost',
		charset='utf8',
		use_unicode=True)
	return conn

def search_form(request):
	try :
		Id = request.session['member_id']
	except KeyError :
		return logout(request)
	
	t = get_template("search_form.html")
	html = t.render(Context({'userId': Id}))
	return HttpResponse(html)


def login(request):
	if 'logId' in request.GET:
		Id = request.GET.get('logId')
		Id = Id.lower()
		if len(Id) <= 0 :
			return logout(request)

		# if there is no id, add

		# if there is id, make a session
		request.session['member_id'] = Id
		Id = request.session['member_id']

		t = get_template("search_form.html")
		html = t.render(Context({'userId': Id}))
		return HttpResponse(html)
	else :
		return logout(request)

def logout(request):
	try:
		del request.session['member_id']
		del request.session['saved_words']
	except KeyError:
		pass
	return render(request, 'login.html')

def simpleListCnt(text,Id):
	resultList = []
	resultcnt = []
	prevWord = "" 
	cnt = 1
	index = 0

	# connect DB
	con = connect_wordDB()
	c = con.cursor()

	for cur in text: 	
		cur = cur.lower()
		if len(cur) <= 1 :
			continue

		if prevWord == cur :
			if index > 0 :
				resultList[index-1].cnt = resultList[index-1].cnt + 1
			continue
		else :
			# diff
			prevWord = cur 

			# check myWordsDB for known word
			queryStr="SELECT known FROM myWords where word = '%s' AND userID='%s'" % (cur,Id)
			c.execute(queryStr)
			knownFlags = c.fetchall()

			if len(knownFlags) :
				pass
				#if knownFlags[0][0] == 0 :
					#index += 1
					#resultList.append(ParsedWord(cur,cnt))
			else :
				index += 1
				resultList.append(ParsedWord(cur,cnt))


	# close DB
	con.close()

	return resultList

def check_known(request):
	wordInput = None
	knownFlag = None

	# check user session
	try :
		Id = request.session['member_id']
	except KeyError:
		return logout(request)

	if 'word' in request.GET:
		wordInput = request.GET.get('word')

		if 'known' in request.GET:
			knownInput = request.GET.get('known')
		else :
			knownInput = 'unknown'

		if 'redirect' in request.GET:
			redirect = request.GET.get('redirect')
		else :
			redirect = 'search'

		if 'meaning' in request.GET:
			note = request.GET.get('meaning')
		else :
			note = ''
		#note = note.encode('utf-8')

		now = time.localtime()
		sqlTime = time.strftime('%Y-%m-%d %H:%M:%S',now)

		knownFlag = 0
		discard = 0
		knownDate = 0
		unknownDate = 0
		if knownInput == 'true' :
			knownFlag = 1
			knownDate = sqlTime
		else :
			knownFlag = 0
			unknownDate = sqlTime

		if knownInput == 'del' :
			knownFlag = 2
			discard = 1

		con = connect_wordDB()
		c = con.cursor()
		c.execute("SET NAMES utf8mb4;") #or utf8 or any other charset you want to handle
		c.execute("SET CHARACTER SET utf8mb4;") #same as above
		c.execute("SET character_set_connection=utf8mb4;") #same as above

		queryStr="SELECt * FROM myWords where word = '%s' AND userID='%s'" % (wordInput,Id)
		c.execute(queryStr)
		rows = c.fetchall()

		if len(rows) :
			if len(note) :
				queryStr = "UPDATE myWords SET known=%d, note='%s' WHERE word='%s' AND userID='%s'" % (knownFlag, note, wordInput,Id)
			else :
				queryStr = "UPDATE myWords SET known=%d WHERE word='%s' AND userID='%s'" % (knownFlag, wordInput,Id)
		else :
			queryStr = "INSERT INTO myWords VALUES ('%s',%d,'%s',3,'%s','%s',%d,0,'%s')" % (wordInput, knownFlag, knownDate, Id, unknownDate, discard, note)

		c.execute(queryStr);
		con.commit()

		con.close()

		if redirect == 'list' :
			return list_mywords(request)

		return search(request)
	
	else :
		return search(request)

def find_meaning(request):
	try :
		Id = request.session['member_id']
	except KeyError :
		return logout(request)

	if 'redirect' in request.GET:
		redirect = request.GET.get('redirect')
	else :
		redirect = 'search'

	if 'q' in request.GET:
		qval = request.GET['q']
	else :
		qval = 'unknown'

	if 'known' in request.GET:
		knownFlag = request.GET.get('known')
	else :
		knownFlag = 'true'
	
	if 'word' in request.GET:
		wordInput = request.GET.get('word')
		if knownFlag == 'true':
			t = get_template("check_meaning.html")
			html = t.render(Context({'wordInput': wordInput, 'userId':Id}))
		else :
			t = get_template("unknown_meaning.html")
			html = t.render(Context({'wordInput': wordInput, 'userId':Id, 'known':knownFlag, 'redirect':redirect, 'qval':qval}))
		
		return HttpResponse(html)
	else :
		if redirect == 'list' :
			return list_mywords(request)

	return search(request)

def load_unknown(Id):
	con = connect_wordDB()
	c = con.cursor()
	query = "SELECT word FROM myWords WHERE userID = '%s' AND known = 0" % (Id)
	c.execute(query)
	unknownWords = c.fetchall()
	con.close()
	return unknownWords

def deco_text(Id, text):
	import re
	import html

	unknownWords = load_unknown(Id)
	decoText = text.lower() 

	for unKnown in unknownWords :
		keyString = unKnown[0].lower()
		repString = '<font color=red>%s</font>' % keyString
		decoText = re.sub(r"\b%s\b"%keyString, repString, decoText)

	return decoText 

def byCnt_key(ParsedWord):
	return ParsedWord.cnt

def search(request):
	import re
	import locale
	import html

	messageList = []
	sortedMessage = []

	try :
		Id = request.session['member_id']
	except KeyError :
		return logout(request)

	if 'inq' in request.GET :
		#remove saved session
		if "saved_words" in request.session : 
			del request.session['saved_words']
		if "saved_text" in request.session : 
			del request.session['saved_text']


		messageInput = request.GET['inq']
		# make pure words content
		parseMsg = re.findall(r"\w+|[^\w\s]", messageInput, re.UNICODE)	

		# sort with duplication allowed
		locale.setlocale(locale.LC_ALL,"")
		parseMsg.sort()

		request.session["saved_words"] = parseMsg 
		request.session["saved_text"] = messageInput
	else :
		try :
			parseMsg = request.session["saved_words"]
			messageInput = request.session["saved_text"]
		except KeyError :
			return search_form(request)

		if 'type' in request.GET:
			viewType = request.GET['type']
			if viewType == 'text' :
				f = get_template("result_text.html")
				decoText = html.unescape( deco_text(Id, messageInput) )
				html = f.render(Context({'text': decoText,'userId':Id }))
				return HttpResponse(html)
			

	# remove duplication and check count
	messageList = simpleListCnt(parseMsg,Id)

	# sort with desending count
	import operator
	messageList.sort(key=operator.attrgetter('cnt'), reverse=True)

	f = get_template("result.html")
	html = f.render(Context({'sort_words': messageList, 'userId': Id}))
	return HttpResponse(html)

def list_mywords(request) :
	if 'q' in request.GET:
		messageInput = request.GET['q']
		if len(messageInput) <= 0 :
			messageInput = 'all'
	else :
		messageInput = 'all'


	try :
		Id = request.session['member_id']

		con = connect_wordDB()
		c = con.cursor()
		query = "SELECT * FROM myWords WHERE userID = '%s' AND known <>2 ORDER BY word" % (Id)
		if messageInput == 'unknown' :
			query = "SELECT * FROM myWords WHERE userID = '%s' AND known = 0 ORDER BY unknownDate DESC" % (Id)
		elif messageInput == 'del' :
			query = "SELECT * FROM myWords WHERE userID = '%s' AND known = 2 ORDER BY word" % (Id)
		elif messageInput == 'known' :
			query = "SELECT * FROM myWords WHERE userID = '%s' AND known = 1 ORDER BY word" % (Id)
		
		c.execute(query)
		words_meta = c.fetchall()
		desc = c.description
		con.close()

		if messageInput == 'unknown' :
			t = get_template("list_unknown_mywords.html")
		elif messageInput == 'del' :
			t = get_template("list_unknown_mywords.html")
		elif messageInput == 'known' :
			t = get_template("list_unknown_mywords.html")
		else :
			t = get_template("list_mywords.html")

		html = t.render(Context({'wordsMeta': words_meta, 'wordsDesc': desc, 'userId':Id, 'qInput':messageInput}))
		return HttpResponse(html)

	except KeyError :
		return logout(request)
